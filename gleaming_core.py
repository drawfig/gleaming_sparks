import discord
from discord.ext.commands import Bot
from discord.ext import commands
import asyncio
import time
import subprocess
import random
import wikipedia
import youtube_dl
from derpibooru import Search

def num_chk(num):
	try:
		check = int(num)
	except ValueError:
		return False
	else:
		return True

def wiki_sum(topic):
	
	found = wikipedia.summary(topic, sentences=1, chars=100, auto_suggest=True, redirect=True)
	return found

Client = discord.Client()
client = commands.Bot(command_prefix = '>')
pass_chk = []
timer_stamp = 0
time_stamps = []
players= {}
queues = {}
voice_clients = {}
connections = []
usr_track = []
like_track = []
triggers = ['GLEAMING', 'BOT', 'ROBOT']

def check_queue(IDs):
	print(queues[IDs])
	if queues[IDs] != []:
		asyncio.run_coroutine_threadsafe(player_generate(IDs), client.loop)
	else:
		del queues[IDs]
		



async def player_generate(IDs):
		ytURL = queues[IDs].pop(0)
		voice_cli = voice_clients[IDs]
		try:
			player = await voice_cli.create_ytdl_player(ytURL, after=lambda: check_queue(IDs))
		except (youtube_dl.utils.ExtractorError, youtube_dl.utils.DownloadError):
			await client.send_message(client.get_channel('316820639270830081'), "Hmm. seems like that wasn't a valid URL...")
			check_queue(IDs)
		else:
			players[IDs] = player
			em = discord.Embed(title=player.title, url=ytURL, color=0xff0000)
			em.set_author(name="Now Playing:", icon_url="https://i.imgur.com/3EspVIK.png")
			await client.send_message(client.get_channel('316820639270830081'), embed=em)
			player.start()

token_container = open('token.txt', 'r')
token = token_container.read(60)
token_container.close()
token.split(":")
token = token[:-1]

phrase_container = open('prr_out.txt', 'r')
phrase_prr = phrase_container.read()
phrase_container.close()
prring = phrase_prr.split(":")
prring.pop()

phrase_container = open('neg_replies.txt', 'r')
phrase_aneu = phrase_container.read()
phrase_container.close()
container = phrase_aneu.split(";")
container.pop()

phrase_container = open('pos_replies.txt', 'r')
phrase_aneu = phrase_container.read()
phrase_container.close()
container2 = phrase_aneu.split(";")
container2.pop()

list_container = open('naughty_list.txt', 'r')
list_helper = list_container.read()
list_container.close()
naughty_list = list_helper.split(":")
naughty_list.pop()

list_container = open('throttle_list.txt', 'r')
list_helper = list_container.read()
list_container.close()
throttle_list = list_helper.split(":")
throttle_list.pop()

list_container = open('pos_dic.txt', 'r')
list_helper = list_container.read()
list_container.close()
pos_word = list_helper.split(":")
pos_word.pop()

list_container = open('neg_dic.txt', 'r')
list_helper = list_container.read()
list_container.close()
neg_word = list_helper.split(":")
neg_word.pop()

phrase_container = open('usr_track.txt', 'r')
phrase_aneu = phrase_container.read()
phrase_container.close()
usr_track = phrase_aneu.split(":")
usr_track.pop()

phrase_container = open('like_track.txt', 'r')
phrase_aneu = phrase_container.read()
phrase_container.close()
like_track_helper = phrase_aneu.split(":")
like_track_helper.pop()
for moodVal in like_track_helper:
	like_track.append(float(moodVal))
	

@client.event
async def on_ready():
	print('READY!')
	await client.change_presence(game=discord.Game(name='with your mind'))

@client.event
async def on_message(message):
	if message.content.upper() == 'HELLO':
		await client.send_message(message.channel, 'World!')

	if message.content.upper().startswith('>PING'):
		userID = message.author.id
		await client.send_message(message.channel, "<@%s> Pong!" % (userID))

	if message.content.upper().startswith('>PUPPET'):
		if str(message.author.id) == '234753676827164674':
			print(message.channel)
			words = message.content.split(" ")
			if len(words) > 1:
				await client.send_message(message.channel, '%s' % (' '.join(words[1:])))
				await client.delete_message(message)
			else:
				await client.send_message(message.channel, "You didn't tell me what to say!")
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")

	if message.content.upper().startswith('>NIGHTY NIGHT'):
		if str(message.author.id) == '234753676827164674':
			await client.send_message(message.channel, "Yawn")
			await client.close(),
			
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")

	if any(key in message.content.upper() for key in triggers):
		post_scr = 0
		if message.author.id not in usr_track:
			usr_track.append(message.author.id)
			like_track.append(0)
		message_con = message.content.upper().split(' ')
		pos = usr_track.index(message.author.id)
		for word in message_con:
			if word in neg_word:
				post_scr = post_scr-1
			elif word in pos_word:
				post_scr = post_scr+1

		like_track[pos] = like_track[pos]+(post_scr/4)
		if post_scr < 0:
			await client.send_message(message.channel, random.choice(container))
		if post_scr > 0:
			await client.send_message(message.channel, random.choice(container2))
		init_recording = open('usr_track.txt', 'w')
		for x in usr_track:
			init_recording.write(x)
			init_recording.write(':')
		init_recording.close()
		init_recording = open('like_track.txt', 'w')
		for x in like_track:
			init_recording.write(str(x))
			init_recording.write(':')
		init_recording.close()

	if message.content.upper() == 'NICE WORK GLEAMING':
		await client.send_message(message.channel, 'Thanks I try!')

	if message.content.upper().startswith('>MUZZLE'):
		if "242822080456949760" in [role.id for role in message.author.roles]:
			trouble = message.content.upper().split(" ")
			order = trouble.index('>MUZZLE')
			trouble = message.content.split(" ")
			if len(trouble) > 1:
				target =  trouble[order+1:]
				target_shoe = (' '.join(target[0:]))
				if target_shoe not in naughty_list:
					naughty_list.append(target_shoe)
			else:
				await client.send_message(message.channel, "You didn't give me a target!")
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.") 

		print(naughty_list)
		init_recording = open('naughty_list.txt', 'w')
		for x in naughty_list:
			init_recording.write(x)
			init_recording.write(':')
		init_recording.close()
	
	if message.content.upper().startswith('>UNMUZZLE'):
		if "242822080456949760" in [role.id for role in message.author.roles]:
			trouble = message.content.upper().split(" ")
			order = trouble.index('>UNMUZZLE')
			trouble = message.content.split(" ")
			if len(trouble) > 1:
				target =  trouble[order+1:]
				target_shoe = (' '.join(target[0:]))
				if target_shoe in naughty_list:
					naughty_list.remove(target_shoe)
				else:
					await client.send_message(message.channel, "No one by that name is muzzled at the moment...")
			else:
				await client.send_message(message.channel, "You didn't give me a target!")
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.") 

		print(naughty_list)
		init_recording = open('naughty_list.txt', 'w')
		for x in naughty_list:
			init_recording.write(x)
			init_recording.write(':')
		init_recording.close()

	if str(message.author) in naughty_list:
		await client.delete_message(message)

	if message.content.upper().startswith('>ECHOES'):
		msg = []
		async for x in client.logs_from(message.channel, limit=2):
			msg.append(x)
		helperID = (msg[1].id)
		words = await client.get_message(message.channel, helperID)
		if words.author.id != "494318200809455626":
			words = words.content
			words = words.split(" ")
			await client.send_message(message.channel, '(((%s)))' % (' '.join(words[0:])))

	if message.content.upper() == 'GLEAMING':
		await client.send_message(message.channel, 'Hi ya!')

	if 'KIRIN' in message.content.upper():
		if message.author.id != "494318200809455626":
			if message.channel.id != '487414502980190238':
				await client.send_message(message.channel, "Kirin? More like... Trash!")

	if message.content.upper().startswith('>THROTTLE'):
		if "242822080456949760" in [role.id for role in message.author.roles]:
			timing = message.content.split(";")
			if len(timing) > 1:
				secs = timing[1]
				if num_chk(secs) == True:
					trouble = message.content.upper().split(';')
					sorting = trouble[0]
					trouble = sorting.split(' ')
					order = trouble.index('>THROTTLE')
					sorting = timing[0]
					trouble = sorting.split(" ")
					print(trouble)
					if len(trouble) > 1:
						if trouble[1] != '':
							target =  trouble[order+1:]
							target_shoe = (' '.join(target[0:]))
							
							if target_shoe not in throttle_list:
								throttle_list.append(target_shoe)
								throttle_list.append(secs)
								time_stamps.append(0)
								pass_chk.append(0)
							print(throttle_list)
					
							init_recording = open('throttle_list.txt', 'w')
							for x in throttle_list:
								init_recording.write(x)
								init_recording.write(';')
							init_recording.close()
						else:
							await client.send_message(message.channel, "You didn't give me a target!")
					else:
						await client.send_message(message.channel, "You didn't give me a target!")
				else:
					await client.send_message(message.channel, "Sorry the seconds you gave me wasn't a valid number!")
			else:
				await client.send_message(message.channel, "You didn't give me the amount of time to Throttle them by!")
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")

	if str(message.author) in throttle_list:
		time_stamp = time.time()
		local = throttle_list.index(str(message.author))
		current_time = float(time_stamps[int(local/2)]) + float(throttle_list[local + 1])
		if pass_chk[int(local/2)] == 0:
			time_stamps[int(local/2)] = time_stamp
			pass_chk[int(local/2)] = 1
		
		if time_stamp < current_time:
			await client.delete_message(message)
		else:
			pass_chk[int(local/2)] = 0
	
	if message.content.upper().startswith('>UNTHROTTLE'):
		if "242822080456949760" in [role.id for role in message.author.roles]:
			trouble = message.content.upper().split(" ")
			order = trouble.index('>UNTHROTTLE')
			trouble = message.content.split(" ")
			if len(trouble) > 1:
				target =  trouble[order+1:]
				target_shoe = (' '.join(target[0:]))
				if target_shoe in throttle_list:
					finder = throttle_list.index(target_shoe)
					finder_helper = throttle_list[finder + 1]
					throttle_list.remove(target_shoe)
					throttle_list.remove(finder_helper)
					finder_helper = time_stamps[int(finder/2)]
					time_stamps.remove(finder_helper)
					finder_helper = pass_chk[int(finder/2)]
					pass_chk.remove(finder_helper)
					print(throttle_list)
	
					init_recording = open('throttle_list.txt', 'w')
					for x in throttle_list:
						init_recording.write(x)
						init_recording.write(':')
					init_recording.close()
				
			else:
				await client.send_message(message.channel, "There doesn't seem to be anyone by that name being throttled...")
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")

	if message.content.upper().startswith('HEY'):
		await client.send_message(message.channel, "Hay is for horses!")

	if "BENIS" in message.content.upper():
		await client.send_message(message.channel, "Oh fug :DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD")
	
	if message.content.upper().startswith('>REALISTIC ELEPHANTS'):
		await client.send_typing(message.channel)
		with open('elephant.webm', 'rb') as elephant:
			await client.send_file(message.channel, elephant)
			await client.send_message(message.channel, 'Miss you Terry <3')

	if message.content.upper().startswith('>HELP'):
		await client.send_message(message.channel, '''
		HELP LIST:
		----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		>Chat help: will list out the text chat commands for Gleaming
		>Voice help: Will list out the voice chat commands for Gleaming
		''')

	if message.content.upper().startswith('>CHAT HELP'):
		await client.send_message(message.channel, '''
		COMMAND LIST:
		----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		>Help: this command brings up a list of commands
		>Ping: plays a game
		>Echoes: Takes the message above and addes (((echoes))) around it.
		>Version: Gives Gleaming's current version.
		>Muzzle [USERNAME]: Mutes the person with the username provided. Note: must be the username with their id number at the end!
		>Unmuzzle [USERNAME]: Unmutes the person with the username provided that was muzzled previosly. Note: must be the username with their id number at the end!
		>Throttle [USERNAME];[INT]: This will limit the person with the username provided but the amount of seconds provided. Note: must be the username with their id number at the end!
		>Unthrottle [USERNAME]: Removes the throttling of the person with username provided. Note: must be the username with their id number at the end!
		>Realistic elephants: Gives you a realist elephant
		>Gleaming tell me about [something you want to learn about]: Gleaming will search Wikipedia for what you asked her to.
		>DB;[Tag]: Gleaming will pull up search results from Derpibooru for the tags you added.(Note: break appart your tag with a semicolon, gleaming supports search with multiple tags like this ">db;tag1;tag2")
		>Alert [Announcment here]: Gleaming will post an announcemnt with your anouncment formatted into an embed. (WARNING! This will ping everyone on the server for the announcment.)
		>Lowp;alert [Announcment here]: Does the same as above without pinging everyone. It is recommended for use on none vital announcments.
		''')

	if message.content.upper().startswith('>VOICE HELP'):
		await client.send_message(message.channel, '''
		COMMAND LIST:
		----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		>Join: Tells Gleaming to join the voice channel you are in
		>Bye: Tells Gleaming to leave voice chat
		>Play [youtube url]: will start a player for the url linked or if one is already playing will add the url linked to the queue
		>Pause: pauses play back of the current video
		>Resume:resumes paused playback
		>Skip: skips the current video for the next one in the queue
		>Clear queue: kills the play back.
		''')

	if message.content.startswith("Remmy"):
		await client.send_message(message.channel, 'Miss you boy.')

	if "BRAA" in message.content.upper():
		await client.send_message(message.channel, '>This intimidates the woman...')
	
	if message.content.upper().startswith('>VERSION'):
		await client.send_message(message.channel, 'Gleaming V0.2.1')
	
	if message.content.upper().startswith('>GIT'):
		await client.send_message(message.channel, 'YUP IT WORKS!')

	if message.content.upper().startswith('GLEAMING TELL ME ABOUT'):
		await client.send_typing(message.channel)
		words = message.content.split(" ")
		if len(words) > 4:
			search_words = (' '.join(words[4:]))
			try:
				foundURL = wikipedia.page(search_words, auto_suggest=True, redirect=True)

			except wikipedia.exceptions.WikipediaException:
				await client.send_message(message.channel, "Uhh I didn't quite understand what you want me to tell you...")
			else:
				foundURL = foundURL.url
				await client.send_message(message.channel, wiki_sum(search_words))
				await client.send_message(message.channel, foundURL)
		else:
			await client.send_message(message.channel, "I don't know what to tell you about!")
	
	if message.content.upper().startswith('>DB'):
		await client.send_typing(message.channel)
		db_img = []
		ava = message.author.avatar_url
		author = str(message.author)
		author2 = message.author
		runner = 0
		page_count = 1
		list_find = 0
		words = message.content.split(";")
		if len(words) > 1:
			words.pop(0)
			search_words = ('+'.join(words[0:]))
			titl = 'Results from DB for: ' + search_words
			for image in Search().filter(filter_id='56027').ls_query(words):
				db_img.append(image.large)
			if db_img != []:
				max_size = len(db_img)
				foota = str(page_count)
				footb = str(max_size)
				footc = foota+"/"+footb
				foot_set = footc
				#print(foot_set)
				em = discord.Embed(title=titl, url=db_img[list_find], color=0x3d92d0)
				em.set_author(name=author, icon_url=ava)
				em.set_footer(text=foot_set)
				em.set_image(url=db_img[list_find])
				msg = await client.send_message(message.channel, embed=em)
				await client.add_reaction(msg, '⏪')
				await client.add_reaction(msg, '⏩')
				await client.add_reaction(msg, '⏹')
				
				while (runner == 0):
					#print("before")
					emo = await client.wait_for_reaction(['⏪', '⏩', '⏹'], timeout=30, message=msg)
					if emo == None:
						runner = 1
						print('ded')
					else:
						#print("after")
						con = '{0.reaction.emoji}'.format(emo)
						reactID = "{0.user.id}".format(emo)
						#print(reactID)
						#mem = '{0.member}'.format(emo)
						if reactID == str(author2.id):
							if con == '⏪':
								if page_count != 1:
									page_count = page_count-1
								
							if con == '⏩':
								if page_count != max_size:
									await client.remove_reaction(message=msg, emoji=con, member=author2)
									page_count = page_count+1
						
							if con =='⏹':
								runner = 1
								print('ded')
						
							list_find = page_count-1
							foota = str(page_count)
							footb = str(max_size)
							footc = foota+"/"+footb
							foot_set = footc
							em = discord.Embed(title=titl, color=0x3d92d0)
							em.set_author(name=author, icon_url=ava)
							em.set_footer(text=foot_set)
							em.set_image(url=db_img[list_find])
							await client.edit_message(msg, embed=em)
							await client.remove_reaction(message=msg, emoji=con, member=author2)
		
			else:
				await client.send_message(message.channel, "Sorry no results were found for that combo of tags!")
		else:
			await client.send_message(message.channel, "I don't have any tags!")
	

	if message.content.upper().startswith('>ALERT'):
		if "242822080456949760" in [role.id for role in message.author.roles]:
			author = str(message.author)
			ava = message.author.avatar_url
			words = message.content.split(" ")
			if len(words) > 1:
				anoun = (' '.join(words[1:]))
				em = discord.Embed(title="Official Aryanne Thread Discord Announcement!", description=anoun, color=0x01662e)
				em.set_author(name=author, icon_url=ava)
				await client.send_message(client.get_channel('247526970534002688'), '@everyone')
				await client.send_message(client.get_channel('247526970534002688'), embed=em)
			else:
				await client.send_message(message.channel, "You didn't tell me what to announce...")
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")


	if message.content.upper().startswith('>LOWP;ALERT'):
		if "242822080456949760" in [role.id for role in message.author.roles]:
			author = str(message.author)
			ava = message.author.avatar_url
			words = message.content.split(" ")
			if len(words) > 1:
				anoun = (' '.join(words[1:]))
				em = discord.Embed(title="Official Aryanne Thread Discord Announcement!", description=anoun, color=0x01662e)
				em.set_author(name=author, icon_url=ava)
				await client.send_message(client.get_channel('247526970534002688'), embed=em)
			else:
				await client.send_message(message.channel, "You didn't tell me what to announce...")
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")

	if message.content.upper().startswith('>REBOUND'):
		if str(message.author.id) == '234753676827164674':
			await client.send_message(message.channel, "Ok! See you in 5 seconds!")
			await client.close()
			subprocess.call(['python3.5 pusher.py'], shell=True)
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")

	if message.content.upper().startswith('>UPDATE'):
		if str(message.author.id) == '234753676827164674':
			await client.send_message(message.channel, "Got it! Grabbing update now!")
			listener = subprocess.call(['python3.5 grabber.py'], shell=True)
			await client.send_message(message.channel, 'DONE!')
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")

	if message.content.upper().startswith('>JOIN'):
		try:
			chan = message.author.voice.voice_channel
			await client.join_voice_channel(chan)

		except discord.errors.InvalidArgument:
			await client.send_message(message.channel, "But you aren't in a voice chat right now!" )
		else:
			connections.append(message.server.id)

	if message.content.upper().startswith('>BYE'):
		serv = message.server
		voice_cli = client.voice_client_in(serv)
		await voice_cli.disconnect()
		connections.remove(serv.id)
		

	if message.content.upper().startswith('>PLAY'):
		serv = message.server
		if serv.id in connections:
			words = message.content.split(" ")
			if len(words) > 1:
				ytURL = words[1]
				voice_cli = client.voice_client_in(serv)
				voice_clients[serv.id] = voice_cli
				if serv.id not in queues:
					queues[serv.id] = [ytURL]
					await player_generate(serv.id)
				else:
					queues[serv.id].append(ytURL)
					await client.send_message(message.channel, "I'll jot it down!")
			else:
				await client.send_message(message.channel, "Uhhh... You didn't give me a youtube link to play...")
		else:
			await client.send_message(message.channel, "But I'm not in voice chat!")

	if message.content.upper().startswith('>PAUSE'):
		playID = message.server.id
		if playID in connections: 
			players[playID].pause()
		else:
			await client.send_message(message.channel, "But I'm not in voice chat!")

	if message.content.upper().startswith('>SKIP'):
		playID = message.server.id
		if playID in connections:
			players[playID].stop()
		else:
			await client.send_message(message.channel, "But I'm not in voice chat!")

	if message.content.upper().startswith('>RESUME'):
		playID = message.server.id
		if playID in connections:
			players[playID].resume()
		else:
			await client.send_message(message.channel, "But I'm not in voice chat!")

	if message.content.upper().startswith('>CLEAR QUEUE'):
		playID = message.server.id
		if playID in connections:
			queues[playID] = []
		else:
			await client.send_message(message.channel, "But I'm not in voice chat!")

	if "PRR" in message.content.upper():
		if message.author.id != "494318200809455626":
			if message.author.id != "273579429605801985":
				butter_chk = message.server.get_member('273579429605801985')
				butter_chk = butter_chk.status
				if str(butter_chk) != 'online':
					await client.send_message(message.channel, random.choice(prring))
				else:
					print('chk works')

	if "MURR" in message.content.upper():
		if message.author.id != "494318200809455626":
			if message.author.id != "273579429605801985":
				butter_chk = message.server.get_member('273579429605801985')
				butter_chk = butter_chk.status
				if str(butter_chk) != 'online':
					await client.send_message(message.channel, "Pr... Oh wait... What's my line on this... Oh right fuck off with that furfag shit it's prr or nothing!")

	if "/)" in message.content:
		if message.author.id != "494318200809455626":
			if message.author.id != "273579429605801985":
				butter_chk = message.server.get_member('273579429605801985')
				butter_chk = butter_chk.status
				if str(butter_chk) != 'online':
					await client.send_message(message.channel, "(\ ")

	if "(\ " in message.content.upper():
		if message.author.id != "494318200809455626":
			if message.author.id != "273579429605801985":
				butter_chk = message.server.get_member('273579429605801985')
				butter_chk = butter_chk.status
				if str(butter_chk) != 'online':
					await client.send_message(message.channel, "/)")

	if "BIG" in message.content.upper():
		if message.author.id != "494318200809455626":
			if message.author.id != "273579429605801985":
				butter_chk = message.server.get_member('273579429605801985')
				butter_chk = butter_chk.status
				if str(butter_chk) != 'online':
					await client.send_message(message.channel, "Crash this plane... oh wait no... For you! Yeah that's it!")
	
	if "DIE" in message.content.upper():
		if message.author.id != "494318200809455626":
			if message.author.id != "273579429605801985":
				butter_chk = message.server.get_member('273579429605801985')
				butter_chk = butter_chk.status
				if str(butter_chk) != 'online':
					await client.send_message(message.channel, "Uhh.. it might hurt a bit...")

	if '<@494318200809455626>' in message.content:
		await client.send_message(message.author, "What's up?")

	if message.content.upper().startswith('>STATUS'):
		if "242822080456949760" in [role.id for role in message.author.roles]:
			words = message.content.split(" ")
			if len(words) > 1:
				status = (' '.join(words[1:]))
				await client.change_presence(game=discord.Game(name=status))
			else:
				await client.send_message(message.channel, "Uhh... You didn't give me a status...")
		else:
			await client.send_message(message.channel, "Sorry you can't pull my strings.")
		
	findit = message.author.id
	if findit in usr_track:
		mood = like_track[usr_track.index(findit)]
		if mood <= -4.0:
			if mood <= -200:
				await client>send_message(message.channel, random.choice(container))
			else:
				repl_chk = (mood*-1)-3
				repl_checker = random.random()*200
				if repl_chk > repl_checker:
					await client>send_message(message.channel, random.choice(container))


@client.event
async def on_voice_state_update(before, after):
	serv = after.server
	if serv.id in connections:
		voi = client.voice_client_in(serv)
		chan_check = voi.channel.voice_members
		IDList = []
		for mem in chan_check:
			IDList.append(mem.id)
		if IDList == ['494318200809455626']:
			await voi.disconnect()
			connections.remove(serv.id)

@client.event
async def on_member_join(member):
	userID = member.id
	if member.server.id == '242773307961769985':
		await client.send_message(client.get_channel('378658197638086656'), 'Hi ya <@%s>!' % (userID))
	
	else:	
		print("how is this possible?")


client.run(token)
